---
layout: single
title: "Trademark Policy Pointers"
permalink: /node/21
---

Trademark policies by FOSS communities:

* [GNOME](http://foundation.gnome.org/licensing/index.html)
* [Ubuntu](http://www.ubuntu.com/aboutus/trademarkpolicy)
* [Eclipse](http://www.eclipse.org/legal/logo_guidelines.php)
* [Mozilla](http://www.mozilla.org/foundation/trademarks/)
* [OpenJDK](http://openjdk.java.net/legal/openjdk-trademark-notice.html)
* [Apache](http://www.apache.org/foundation/licence-FAQ.html#Marks)
* [Subversion](http://subversion.org/legal/trademark-policy.html)
* [Python](http://www.python.org/psf/trademarks/)
* [Perl](http://www.perlfoundation.org/perl_trademark)
* [Linux Mark Institute](http://www.linuxmark.org/)
* [OSI](http://www.opensource.org/trademark)
