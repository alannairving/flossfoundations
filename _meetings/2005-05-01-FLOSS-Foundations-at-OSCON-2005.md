---

layout: single
title: "FLOSS Foundations at OSCON 2005"
date: 2005-05-01
permalink: /flossfoundations/node/12
categories:
    - meetings
---
The summit is a place for the people who try to Get Stuff Done at the various non-profits around open source to congregate, share information, and pick each others' brains. This was the first FLOSS summit.

## When? Where?

July 30 and 31 in Portland, OR. The summit was held immediately before [OSCON](http://conferences.oreillynet.com/os2005), at the [Oregon Convention Center](http://www.oregoncc.org/).

## Who?

Final attendee list:

* Allison Randal, president of the Perl Foundation (organizer of the summit)
* David Ascher, director of the Python Software Foundation
* Josh Berkus, core member of PostgreSQL
* Paula Le Dieu (Creative Commons)
* Paul Everitt, Plone Foundation
* Roberto Ierusalimschy, creator of Lua
* Scott Kveton, Open Source Lab
* Ted Leung, Open Source Applications Foundation
* Mike Milinkovich, Eclipse Foundation
* Bill Odom, steering committee chair of the Perl Foundation
* Russ Nelson, board member of OSI
* Alex Russell, Dojo Foundation
* Chris Messina, Round Two
* Corey Shields, Gentoo Linux Foundation
* Donnie Berkholz, Gentoo Linux Foundation (Sunday only)

For the legal section (Sunday only):
 
* Roberta Cairney
* Larry Rosen
* Cliff Schmidt, ASF

Unable to attend:

* David Neary, GNOME Foundation
* Brian Behlendorf, Apache Software Foundation regrets, prior plans
* Theo de Raadt (OpenBSD, OpenSSH)
* Greg Stein, ASF sorry, arriving Tuesday
* Mark Wielaard (Classpath Leader)
* Eben Moglen
* Andrew St. Laurent
* Bdale Garbee, SPI / Debian regrets (family conflict)
* Sally Whitehead, Sophos regrets
* Linus Torvalds, Linux / OSDL regrets (family conflict)
* Danese Cooper, board member of the Open Source Initiative regrets (family conflict)
* Michael Tiemann, President of OSI regrets
* Mitchell Baker, Mozilla Foundation regrets
* Ton Roosendaal, Blender Foundation regrets
* Jeff Waugh, Ubuntu / GNOME regrets, arriving Monday
* Tim Ney, GNOME Foundation regrets
* Geir Magnusson, ASF regrets
* Andrew Morton, Linux / OSDL
* Bruce Perens, SPI
* Sam Ruby, Apache Software Foundation
* Bruno Souza (SouJava President - large NGO in Brazil)
* Jimmy Wales, Wikimedia
* Bradley Kuhn (FSF)
* Jeffrey Thompson, IBM

Other suggested invitations:

* Head of Mambo Project (incorporating soon)
* Someone from FreeBSD
* someone from EFF?
* someone from FSF (as distinct from SFLA)
* Someone from KDE (Aaron Seigo?)

## Agenda

The first two topics were discussed on the first day, and the last two topics on the second day.

### Non-Profit Status: Community

* How do different projects manage themselves?
* What works? What doesn't?
* What problems have you encountered that others have solved?
* Designing for success...engagement models for communities
* Relationships between Boards and developer communities.
* Corporate participation in foundation leadership.
* What policies do you need to have in place to herd the cats?
* How can we better work together?
* What possibilities are there for inter-foundation collaboration?

### Donors

* What do they want?
* How to deal with them.
* How to make them happy without selling out.
* Revenue models for non-profits.

### Legal Issues: Contributing, Licensing, and Intellectual Property

* Working with communities to develop strategies suited to specific needs.
* Tools for keeping projects open and productive.
* Tools for decreasing risks of lawsuits (contributor agreements), etc.
* Trademark guidelines - can guidelines for the use of trademarks be legally rigorous, readable and sufficiently permissive for community group needs?
* Developing and servicing a trademark
* Contract negociation (part of trademark development)

### Non-Profit Status: Legal

* Mixing business and charity: what Board member activities can get you in hot water.
* Can 501(c)(3)s and equivalent fund OSS development?
* Going international: how to create a multi-region non-profit.
* D&O Insurance...do you need it?
