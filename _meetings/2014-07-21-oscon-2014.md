---
layout: single
title: "OSCON2014"
date: 2014-07-21
permalink: /flossfoundations/oscon2014
categories:
    - meetings
---

Hi all!

If you are bringing a guest put +1 right after your name. And if you have any kind of dietary restrictions, put it in parentheses after your name. You can also email me with your information and I'll add you to one or both of the proposed meals. deb at eximiousproductions dot com

Cheers,

Deb

Monday 7/21, dinner at 7pm

At the Burnside Brewing Company (http://www.burnsidebrewco.com)

701 E. Burnside Ave. which is 11 mins walking or 7 minutes trolley from the Convention Center

1. Deb Nicholson
2. Simon Phipps
3. Chris Aniszczyk
4. Mike Milinkovich
5. Allison Randal (gluten-free, vegan, salad++)
6. Karl Fogel
7. Leslie Hawthorn
8. Louis Suarez-Potts
9. Stefano Maffulli +1
10. Michael Downey
11. Aaron Wolf
12. Danese Cooper
13. Steve Holden
14. Josh Berkus (non-meat, fish OK)
15. Rich Sands
16. Karen Sandler
17. Bradley M. Kuhn (arriving late, likely for dessert)
18. Steven Muegge
19. Shane Curcuru

Tuesday 7/22, breakfast at 7:45am

At the Douglas Fir Lounge (http://www.dougfirlounge.com/)

830 E Burnside St, which is 11 mins walking or 7 minutes trolley from the Convention Center

Please bring cash since the restaurant doesn't want to run more than 6 cards per party.

1. Deb Nicholson
2. Bradley Kuhn (vegetarian)
3. Justin Erenkrantz
4. Alolita Sharma
5. Allison Randal (coffeetarian, soymilk?)
6. Leslie Hawthorn
7. Ruth Suehle
8. Brian Behlendorf
9. Dave Neary
10. Cat Allman
11. Michael Downey
12. Aaron Wolf
13. Jono Bacon
14. Louis Suarez-Potts
15. Chris DiBona
16. Karl Fogel
17. Chris Aniszczyk
18. Deb Bryant
19. Karen Sandler
20. John A. Lewis
21. Stormy Peters
