---
layout: single
title: "FLOSS Foundations at FOSDEM 2007"
date: 2007-02-01
permalink: /flossfoundations/node/5
categories:
    - meetings
---

A Foundations dinner was held Saturday, February 24 at [FOSDEM](https://fosdem.org) (February 24 and 25, 2007 in [Brussels, Belgium](http://maps.google.com/maps?ie=UTF8&z=17&ll=50.812375,4.380734&spn=0.005369,0.011373&om=1)).

FOSDEM Attendees

Allison Randal – a Perl Foundation Director, works for O'Reilly.

Greg Stein – current Apache Chairman, works for Google.

Sander Striker – current Apache President, works at Joost. Scheduled arrival on Saturday.

Thom May – an Apache Member.

Nick Kew – an Apache Member.

Gervase Markham - Mozilla Foundation employee.

Wytze van der Raay – Stichting NLnet financial advisor

Zaheda Bhorat – OpenOffice.org and Google

Dave Neary – the GNOME Foundation and OpenWengo

Cornelius Schumacher – KDE e.V., works at Novell.

Sebastian Kuegler – KDE e.V.
