---
layout: single
title: "FLOSS Foundations at EuroOSCON 2005"
date: 2005-05-01
permalink: /flossfoundations/node/11
categories:
    - meetings
---

The Free/Libre Open Source Software Project Summit aims to be a place for the people who try to Get Stuff Done at the various non-profits around open source to congregate, share information, and pick each others' brains. This is the second FLOSS summit, following in the vein of the original summit.

## Notes

The notes for this meeting are below. Please review and correct these notes as appropriate.

## When? Where?

Friday, October 21st in Amsterdam, [update: 9AM] The summit will be held immediately after [European OSCON](http://conferences.oreillynet.com/cs/eurooscon/create/e_sess).

The meeting location is: the 15th century Waag building, Nieuwmarkt Amsterdam, in the "Theatrum Anatomicum", which is the top floor theatre. [How to get there](http://web.archive.org/web/20061020105208/http://www.blender3d.org/cms/Venue__travel___housin.374.0.html).

The entrance is at "De Waag Society", a smaller door in one of the towers of de Waag building, to the right of the main entrance (which is restaurant only). Just ring the doorbell and say you're there for the "Foundations Summit". Then just go up two levels to the 2nd floor.

## Who?

Present:

* Ton Roosendaal (ton@blender.org), Blender Foundation
* Eva Brucherseifer (eva@kde.org), KDE e.V.
* Wytze van der Raay (wytze@nlnet.nl), Stichting NLnet
* Russ Nelson, OSI & Public Software Fund
* David Neary, Gnome Foundation
* Mark Wielaard, GNU Classpath
* Allison Randal, Perl Foundation
* Johan Janssens, Joomla! (Open Source Matters)
* Nicolas Steenhout (I think), Joomla!
* Jim Fruchterman (jim@benetech.org), Benetech
* Stefan Taxhet (st@openoffice.org), Sun and OpenOffice.org
* Gervase Markham (gerv@mozilla.org), Mozilla Foundation
* Chris Messina, Web 2.0
* Martin v. Löwis, Python
* Paula Le Dieu, Creative Commons International

Space is limited to 20 people. The initial invitation list is:

* _confirmed_ David Neary, GNOME Foundation (arriving Friday around midday)
* Christophe Ney, ObjectWeb
* Paul Everitt, Plone Foundation
* Ann Barcomb, YAPC Europe
* _invited_ Norbert Gruener, YAPC Europe Foundation
* _confirmed_ Jim Fruchterman, Benetech (arriving Friday noonish)
* _invited_ Brian Behlendorf, ASF

Please suggest others to invite:

* Greg Stein, Apache Software Foundation going to EuroOSCON, _unclear on exact timing_
* Jimmy Wales, Wikimedia _probably won't make it_
* Florence Nibart-Devouard, Wikimedia _Jimmy will ask her_
* Angela Beesly, Wikimedia _Jimmy will ask her_
* Egbert Eich, X.Org (board member) _Eva will ask him_

Regrets:

* Scott Kveton, OSU Open Source Lab
* _not attending_ Danese Cooper, OSI & Intel
* _not attending_ Mitchell Baker, Mozilla Foundation
* _not attending_ Matthias Ettrich, KDE
* _not attending_ Bdale Garbee, SPI
* _not attending_ Cliff Schmidt, Apache Software Foundation
* _not attending_ Tristan Nitot, Mozilla Europe (regrets, family engagements)
* _not attending_ Mirko Boehm, KDE e.V

## Agenda

Each of the following suggested topics is approximately half a day. Feel free to add alternate topics and discussion points under each.

### Introductions

* What is your foundation? How are you organized?

### Non-Profit Status: Community

* How do different projects manage themselves?
* What works? What doesn't?
* What problems have you encountered that others have solved?
* Designing for success...engagement models for communities
* Relationships between Boards and developer communities.
* What policies do you need to have in place to herd the cats?

### Donors

* What do they want?
* How to deal with them.
* How to make them happy without selling out.

### Legal Issues: Contributing, Licensing, and Intellectual Property

* Working with communities to develop strategies suited to specific needs.
* Tools for keeping projects open and productive.
* Tools for decreasing risks of lawsuits (contributor agreements), etc.
* Trademark guidelines - can guidelines for the use of trademarks be legally rigorous, readable and sufficiently permissive for community group needs?
* Developing and servicing a trademark

### Non-Profit Status: Legal

* European non-profit law.
* Multi-region non-profits.

### Conferences

* Concepts
* Good locations
* Experiences

What is this group? Do we want to formalize in some way?

## Notes on FLOSS Foundations meeting on October 21, 2005 at De Waag, Amsterdam

Attendees:

* Ton Roosendaal, Blender Foundation
* Eva Brucherseifer, KDE e.V.
* Wytze van der Raay, Stichting NLnet
* Russ Nelson, OSI & Public Software Fund (left around noon)
* David Neary, Gnome Foundation
* Mark Wielaard, GNU Classpath
* Allison Randal, Perl Foundation
* Martin v. Loewis, Python Software Foundation
* Paula Le Dieu, Creative Commons International (left around 13:00)

Joining in the course of the meeting:

* Jim Fruchterman, Benetech
* Stefan Taxhet, Sun and OpenOffice.org
* Gervase Markham, Mozilla Foundation
* Chris Messina, Web 2.0
* Johan Janssens, Joomla! (Open Source Matters)
* Laurens Vandeput, Joomla!

Primary note taker: Wytze van der Raay

Corrections and additions: ???

Meeting started at 10:00 with an introduction round. Due to the extended introductions and the many questions and exchanges taking place during the introduction round, we didn't do anything else until the meeting closed around 17:00. The following is an attempt to summarize the introduction by each attendee. Please correct any mistakes you find!

### Russ Nelson - founding board member OSI

OSI will start work in 2006 on an 'open source standard'. OSI has 9 board members (some of them international, e.g. Rishab Aiyer Ghosh at Merit in Maastricht, and Sanjiva Weerawarana in Sri Lanka, and Bruno Souza in Brazil). Each year 3 board members are elected, for a 3 year term. The voting is based on an internal selection to ensure representativity for the community. OSI is a not-for-profit corporation under US law, with 501.c3 status for tax purposes. Some discussion about D & O insurance (Director and Officer insurance) – does OSI have this or not? Directors and officers are not liable in the USA, unless they commit malicious practices. OSI does not have any employees, but only volunteers who get expenses reimbursed. Its income is from (mainly corporate) donations, in the range of 10K - 100K US$/year. A board member spends abount 10 hours per month on OSI work excluding meetings.

### Mark Wielaard - GNU Classpath maintainer

Mark is currently mainly a user of foundations, in particular the FSF. He observes that developers are cooperating more easily than foundations. The GNU Classpath project has now about 80 developers, with 5 companies around it. It may need a foundation of its own? FSF holds the copyright, tracks paperwork, answers legal questions, provides Savannah infrastructure etc. About 5 to 6 developers are paid by their company to work on code. Currently there is no formal structure. For instance, Debian/SPI was used recently to handle the finance of a GNU Classpath meeting. Mark finds that some people are too attached to their own foundation, e.g. Apache does not want to talk to FSF?? Most of the formalities is in diligent assignment of copyrights to FSF. Dave Turner and another parttime guy at FSF are handling this. FSF is felt by some people as too political, which makes things difficult sometimes.

### Allison Randal - The Perl Foundation

The Perl Foundation's board consists of 5 persons, half of them active and half of them advisory. Most of the actual work is done by committees with delegated powers. 'Jobs' on committees are rotated every few years if possible to keep things interesting. A spin-off organisation of TPF is the YAPC::Europe Foundation (YEF) for organising Perl conferences in Europe – this is a Dutch foundation with a French bank account. TPF would like to have a board member from Europe. The task of the board is to oversee things; the president of the board takes part in committees. Perl development is not run by TPF, but TPF does fund developers, channelled through a grants committee. Perl development is headed by Larry Wall and a collection of pumpkins under him. The TPF grants committee selects from a list of proposals, varying in size from 2K - 75K US$. The money is obtained from donations (e.g. a recent donation by Morgan Stanley of 35K US$). Grants are passed out free of taxes by TPF. Grant voting takes place each quarter, and is depending on the available budget. TPF has no employees. The Perl5 copyright is owned by Larry Wall, the Perl6/Parrot copyright will be owned by TPF. Thus Perl6 is also a legal cleanup ... Total TPF budget is about 150K US$ per year. A number of Google's 'Summer of Code' grants were obtained by Perl development projects. The actual payout by Google was only 3000 US$ instead of the promised 4500 US$ due to US tax issues.

### Stephan Taxhet - Team OpenOffice.org e.V.

Stephan works for Sun Microsystems. The Team OpenOfficce.org e.V. was founded in 2003 to run an OpenOffice.org conference independent of Sun. Currently this foundation is not very active, there are abount 20 members and 3 board members. They are thinking about how a foundation could work out for OOo. 90-95% of OOo development is done by Sun employees (100 - 150 people). Specific OOo activities are: documentation (Sun does StarOffice docs) and marketing. Other companies (e.g. Novell, Google) are willing to participate in OOo work. They are willing to donate manpower and/or money, but not to Sun. So a separate OOo foundation could be useful here. For OOo development there exists:

* a technical steering committee
* a community council - advisory board

Sun Microsystems is the official copyright holder, the only one who can change the license.

### Paula Le Dieu - Creative Commons International

CC is responsible for the Create Commons licenses ('culture in digital space'). These licenses are now also applied to off-line materials. The licenses are actively 'ported' to many different languages / cultures / countries. Currently there are around 20 translations, and this will be 70 at the end of this year. Local participation in free culture has been boosted by this localization effort. Paula is now moving to iCommons - 'the equivalent of coding communities'. CC has a fairly complex legal structure, not surprising considering the number of lawyers involved in creating and running the organisation ... In the USA CC is incorporated as a 503c non-profit, employing about 8 staff members. The 503c status is reviewed after 3 years by the tax authorities; an important test is whether the organisation has garnered sufficient 'public support' – this is not fulfilled when only a few large grants from other foundations have been received (Ford Foundation, Open Society Institute, ... are currently large donors). CC International is incorporated in London, and has obtained charitable status in the UK. Paula is director of this one. The reason for this separate entity is to show international presence and gain the ability to attract EU money and support EU projects. Right now about 3000 people world-wide identify with the Creative Commons community. CC International is looking for possibilies to team up with other foundations in order to attract EU funds etc., and share the administrative load (overhead estimated at 40% :-( ). A discussion ensues about the difficulties of EU funding and consortia. CCI would also like to do a 'small grants process' like TPF.

### Chris Messina - Web 2 / Umbrella

Chris is looking to setup a foundation to cover a number of developer communities vaguely described by "Web 2.0". This includes things like Ruby on Rails, Drupal, ... (please complete the list). The foundation should provide financial and legal support. Since the developer communities are very international, questions have arised like: where to base? what format? consortium? foundation? Typical for these projects is that they do not receive a lot of corporate support, but rather feature lots of small contributors. A short 48-hour fundraising for 3.000 raised 11.000, ending up in the bank account of a Belgian PhD student, causing subsequent tax troubles ... a clear illustration of the need for a formal entity.

### Ton Roosendaal - Blender Foundation

Ton Roosendaal is the original of the Blender software, which is now open sourced. The Blender Foundation was started 3 years ago. They gathered 100K EUR within 7 weeks in order to buy the source code rights for Blender after the company went bankrupt. The foundation is now making money by publishing books; this is enough to fund a small organization and office. It appears to be growing into a small services company. The community (not the office) develops the software. There is no voted board, but only a formal board. Ton wants to channel community input through some kind of advisory board. They are also organizing conferences, and setting up some projects (example: movie produced under Creative Commons license). The foundation has about 50K euro/year to spend. The movie project costs 120K euro, which is partly financed by innovation subsidy (free of income tax?) and subsidies from various Dutch culture foundations.

Chris Messina brings up a discussion about the lack of good tools to involve many more users in development and graphical design: social contact is very useful to involve/motivate more people to contribute actively. This is based on experiences from Firefox development. Some kind of "Social SourceForge 2.0" would be needed. Can we write an 'Open Source Foundation Best Practices' guide??

Ton Roosendaal: we also need an approach for involving professional developers/companies. Others are of the opinion that this will automatically organize itself when the software is successful. We might need a balance between the fully non-commercial approach, limited service providing or just making profit. One way to structure this is to setup a separate company which is owned by the foundation (to be illustrated later by Mozilla ...).

### Martin von Loewis - Python Software Foundation

The Python Software Foundation (PSF) is based in Virginia, USA. Martin is one of the 7 board members. They organize conferences, operate a webserver and try to collect copyrights for the Python code base. They would like to simplify the Python licensing, which is currently complicated due to the previous employers of Guido van Rossum (CNRA, CWI, ...). PSF has a number of committees, which tend to become inoperative after a while. The exception for this is the conference committee. The work is done by the board members. PSF has about 60 regular members (based on a merit system) and about 20 sponsor members. PSF has no control over development. Its primary role is to take financial risk, something that cannot be done by volunteers. They also have some money now for grants (about 40K US$), but this is not continuously, and very depending on the financial results of the conferences. There is no paid staff, only volunteers. PSF also runs a job board for company recruiting.

### Eva Brucherseifer - KDE e.V.

KDE e.V. was started in 1997 to handle the finance for the KDE project. Eva is one of the 4 board members. KDE e.V. has become more active in the last 3 years. There is a merit-based system for active members who have voting rights for the board. Proxy voting is possible, so people don't have to be physically present for voting. Sponsoring members (and patrons) are possible, but not yet present. The last 3 years have seen more confereneces and more legal stuff. Since one year the technical stuff is also under voting control of KDE e.V. Currently there are about 120 members, but many many more (1500?) contributors world-wide. They are now establishing working groups to structure activities. Under German law board members cannot be employed by the e.V., so they have to be volunteers. They want more local groups in the various countries (France, NL, USA). They have trademarks (USA and Europe) for protection purposes; there are liberal rules for usage of the trademarks. They are thinking about participating in EU-funded work (Adriaan de Groot at the Radboud University Nijmegen is the contact for this). The CVS server for KDE is supported by SuSE, which also has 15 KDE developers on its payroll. German universities are supporting some KDE servers. They do not yet have a 'front end' for companies who want to talk to the KDE 'company'. This is also because KDE e.V. cannot have too much 'business' activity within the e.V. due to its legal/fiscal status; receiving sponsor money already falls under 'business' activity, thus leaving little room. Gervase Markham: for this reason Mozilla was split up!

### Wytze van der Raay - Stichting NLnet

Stichting NLnet was set up in 1989 to take over the Dutch UUCP network operations from CWI. It soon evolved into the first ISP in Holland. After 5 years the non-profit foundation was felt too limiting for this work, thus a commercial entity (BV under Dutch law) was setup with its shares owned by the foundation. In 1997 the BV including all ISP activities was sold to UUNET. This provided the starting capital for the 'second life' of NLnet starting in 1998. NLnet is a non-profit foundation with 5 board members. Three of the board members are employed by the foundation (one of them part-time). The goal is to stimulate network research and development in the internet technology domain. All results of projects sponsored by NLnet must be made freely available to the community. NLnet has a preference for the GNU GPL/LGPL/FDL licenses, but is not dogmatic about it – BSD licenses are used in some cases where this is more practical. The amount of money available per year is around 1 to 2 million euro. NLnet supports two long-term on-going projects: a university research group (IIDS == Intelligent Interactive Distributed Systems) at the Vrije Universiteit, and a network engineering laboratory NLnet Labs, also based in Amsterdam, and employing about 6 people, active in areas like DNSSEC, IPv6, HIP and related internet naming & routing technology. Each of these projects consumes about 400K euro per year. The remaining money is used for a variety of shorter projects – please check the website at www.nlnet.nl for a good overview. NLnet has a 'charity' status for tax purposes, but is not attracting money through donations – the invested capital (currently around 25M euro) generates a reasonable stream of income. NLnet was afraid to be overwhelmed by proposals when they started, but this didn't happen. Its existence sometimes seems a well-kept secret, but this is not intentional. There is no fixed procedure for submitting proposals; the best procedure is to get in touch with one of the board members and submit a small overview of an intended project. NLnet likes to work out the details with the proposer over time – or tell him/her to go elsewhere if they are not interested. Gervase Markham is interested to discuss NLnet's project selection policy as the Mozilla Foundation expects to be confronted with this problem real soon now.

### David Neary - GNOME Foundation

Note: there is also a Gnome e.V. (in Germany). David considers keeping the foundation relevant to its underlying community to be one of the main challenges facing the GNOME foundation. The foundation does stuff that doesn't get done otherwise. The foundation consists of developers, companies and users. A membership committee decides who can be a member (based on contributions to the project). Currently there are around 380 members. Members can vote for the board. The foundation has 501c3 status. It manages the trademark (can be used under various licenses). It does not manage the copyrights for Gnome; some of these are held by the FSF. In addition to the member-voted board of directors there is an advisory board, which consists of 8 - 9 paying members (Sun, IBM, HP, Novell and RedHat are the major ones) and provides the primary source of recurring income for the foundation, about 70K US$/year. This is enough to fund one full-time person, dedicating 60% to fund-raising activities, and 40% to the organization of Guadec. Other income is obtained from sponsorship for the Guadec conferences. Tim Ney is the executive director, and the only salaried employee of the foundation. The advisory board is not very active now, it meets once a year at Guadec. Dave would like to get it more involved somehow.

### Johan Janssens - Open Source Matters Foundation (Joomla!)

The Open Source Matters Foundation was just recently (september 2005) incorporated in the USA a s a legal entity. This is a result of the Mambo (CMS) problem – a dispute arose with the company that started Mambo (Miro) over moving the Mambo copyright and trademark to an independent Mambo foundation. As a result the entire core team stepped out of the project and set up a new project with a new name (Joomla!), since Miro owned all the servers etc. VA Software donated the SourceForge Enterprise software + maintenance which is now used for development (but this is closed source stuff!). The Software Freedom Law Center (SFLC) provides legal advice, pro bono. Johan recommends them. They are still working on setting up the exact foundation rules ... The new software release still acknowledges shared copyright of Miro in a single notice (not necessary to do this in every file).

Eva remarks that distributed copyright effectively protects KDE against a possible take over. Gervase remarks that copyright assignment is more important for projects that do not use GPL, but might need to become compatible with GPL some time in the future.

### Jim Fruchterman - Benetech

The Benetech social company is 16 years old, and was started as a spinoff for producing reading machines for blind people. Benetech is deliberately non-profit and wants to deliver technology applied to social project. It has 501c3 status. In 2000 they sold the reading machines for the blind business for 3M US$ cash plus royalties plus some consulting contracts. A for-profit consulting company has been split off from Benetech for tax reasons. They have about 5M US$ available for new projects. Currently there are 6 projects for social sector / human rights organisations. All of these are open source. An example is providing usable crypto for grassroots groups. The target customers are non-technical, so not contributing themselves to open source software development. With a 3M US$ yearly budget about 20 people are employed and 6 - 10 consultants world-wide working part time. They also have a lot of volunteers (mainly for scanning of books). The board consists of 5 people, 4 outside non-paid, and 1 inside paid, i.e. Jim. The yearly income is about 1M from 'sales', 1M - 1.5M from donations by rich individuals, and 1M from donations by 'traditional' foundations. They are struggling to comply with the US tax laws.

### Gervase Markham - Mozilla Foundation

In 2003 the Mozilla Foundation was spun off from AOL/TimeWarner/Netscape, with 1M US$/year for two years seed funding. In 2005 it was split up into the Mozilla Corporation (employing 40-odd people) and the Mozilla Foundation, for tax reasons. The foundation employs 3 people: Frank Hecker (part-time), Zak Greant, and Gervase (2 days/week). Because this restructuring is very recent, the income for the foundation is not quite clear right now, but the budget will probably somewhere on the order of 1M US$/year. They still need to work out how to spend any extra money beyond base operational expenses (through grants or otherwise). The Mozilla codebase is shared copyright, but the Foundation owns the trademarks, which are licensed to the corporation. All the shares of the corporation are owned by the foundation. The corporation has a board of 5, the foundation a board of 3 (non-paid). The foundation's board chooses itself (there are no voting members). The development structure recognizes:

* staff
* drivers (who direct development)
* superreviewers (the higher of the two levels of code review)

Not all of these people are employed by the corporation, based on competence. Sun Microsystems has an engineering team of 50 people in China, all working on Mozilla. IBM is also a key contributor, but Gerv thinks they are ramping down (5 - 6 people). Google employs a number of important Mozilla developers.

Remark from others: people who start working for Google seem to disappear from the open source development world ...

How does Mozilla connect to the community?

* developers: IRC, mailing list, bug tracker
* marketing: spreadfirefox
* mozillazine: community new and forum

It is felt that these meetings are a useful exchanges of experiences between the participant, thus we should continue with this. A number of possible meeting places/times is listed on the wiki (generally one day before or after a relevant conference). The next opportunity in Europe is FOSDEM, which is held February 25/26, 2006 in Brussels.
